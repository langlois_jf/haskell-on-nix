{-# LANGUAGE OverloadedStrings #-}
module Main where

import API

import Prelude

import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.Text
import Formatting
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Options.Applicative
import Servant.API
import Servant.Client

(getNum :<|> putNum :<|> getPerson) = client api

newtype GetCommand = GetCommand Int

newtype PutCommand = PutCommand Int

newtype GetPersonCommand = GetPersonCommand Text

data Config = Config {
  host :: String,
  port :: Int,
  subcommand :: Command
}

data Command = Get GetCommand | Put PutCommand | GetPerson GetPersonCommand

getCommand :: Parser GetCommand
getCommand = GetCommand <$> argument auto (metavar "NUMBER")

putCommand :: Parser PutCommand
putCommand = PutCommand <$> argument auto (metavar "NUMBER")

getPersonCommand :: Parser GetPersonCommand
getPersonCommand = GetPersonCommand <$> argument str (metavar "NUMBER")

commandParser :: Parser Command
commandParser = subparser (
  command "get" (info (Get <$> getCommand) (progDesc "Get number from server"))
  <> command "put" (info (Put <$> putCommand) (progDesc "Check for number parity"))
  <> command "person" (info (GetPerson <$> getPersonCommand) (progDesc "Get a person by ID")))

configParser :: Parser Config
configParser = Config <$> strOption (long "host" <> metavar "HOST" <> value "localhost")
  <*> option auto (long "port" <> metavar "PORT" <> value 8000)
  <*> commandParser

runClient :: (MonadIO m, Show a) => BaseUrl -> ClientM a -> m ()
runClient baseUrl action = liftIO $ do
  manager <- newManager defaultManagerSettings
  res <- runClientM action (mkClientEnv manager baseUrl)
  case res of
    Left err -> fprint ("Error: " % shown) err
    Right response -> print response

main :: IO ()
main = do
  config <- execParser (info configParser (progDesc "Blurb"))
  let
    baseUrl = BaseUrl Http (host config) (port config) ""
    go :: (MonadIO m, Show a) => ClientM a -> m ()
    go = runClient baseUrl
  case subcommand config of
    (Get (GetCommand num)) -> go (getNum num)
    (Put (PutCommand num)) -> go (putNum num)
    (GetPerson (GetPersonCommand num)) -> go (getPerson num)
