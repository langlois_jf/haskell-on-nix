{-# LANGUAGE OverloadedStrings #-}

module Main where

import API
import API.Person

import Formatting
import Network.Wai (Application)
import Network.Wai.Handler.Warp (run)
import Options.Applicative
import Servant.API.Generic (toServant)
import Servant.Server.Generic (AsServer, genericServe)
import System.Environment (lookupEnv)

routesHandler :: Routes AsServer
routesHandler = Routes {
  _get = pure . show,
  _put = pure . odd,
  _person = toServant personHandler
}

personHandler :: PersonAPI AsServer
personHandler = PersonAPI {
  _personGet = pure . (\id -> Person id "" 0)
}

app :: Application
app = genericServe routesHandler

newtype Config = Config {
  port :: Int
}

configParser :: Parser Config
configParser = Config <$>
  option auto (long "port" <> short 'p' <> metavar "PORT" <> value 8000)

main :: IO ()
main = do
  config <- execParser (info configParser (progDesc "Server"))
  fprint ("Running on port " % int % "...\n") (port config)
  run (port config) app
