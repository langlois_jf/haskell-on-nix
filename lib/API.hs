{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module API where

import Data.Proxy (Proxy (..))
import Servant.API
import Servant.API.Generic

import API.Person

data Routes route = Routes {
  _get :: route :- Capture "id" Int :> Get '[JSON] String,
  _put :: route :- ReqBody '[JSON] Int :> Put '[JSON] Bool,
  _person :: route :- "person" :> ToServantApi PersonAPI
} deriving (Generic)

api :: Proxy (ToServantApi Routes)
api = genericApi (Proxy :: Proxy Routes)
