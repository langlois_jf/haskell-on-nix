{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module API.Person where

import Data.Aeson (FromJSON, ToJSON)
import Data.Proxy (Proxy (..))
import Data.Text
import Servant.API
import Servant.API.Generic

newtype PersonAPI route = PersonAPI {
  _personGet :: route :- Capture "id" Text :> Get '[JSON] Person
} deriving (Generic)

data Person = Person {
  _id :: Text,
  _name :: Text,
  _age :: Int
} deriving (Generic, Show)

instance FromJSON Person
instance ToJSON Person
