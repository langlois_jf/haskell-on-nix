{
  compiler ? "default",
  pkgs ? import <nixpkgs> {},
  hie ? import (fetchGit "https://github.com/domenkozar/hie-nix.git") {}
}:
let
  hp = if compiler == "default" then pkgs.haskellPackages else pkgs.haskell.packages."${compiler}";
  hlib = pkgs.haskell.lib;
  pkg = hp.developPackage {
    root = ./.;
    overrides = self: super: {
      ghc = super.ghc // { withPackages = super.ghc.withHoogle; };
      ghcWithPackages = self.ghc.withPackages;
    };
    # This is here to address the fact that HIE/ghc-mod expects
    # Cabal 2.4.0.1, but the built in version is 2.2.0.1
    modifier = drv: hlib.addBuildDepend drv hp.Cabal_2_4_0_1;
  };
in pkg.overrideAttrs (old: {
  buildInputs = old.buildInputs ++ (with hp; [
    cabal-install
    hie.hies
  ]);
})
